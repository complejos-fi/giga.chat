package mx.unam.giga.chat;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import mx.unam.giga.gui.FileDestination;
import mx.unam.giga.gui.Usuario;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class initChatUsuario {
    public initChatUsuario(Usuario user1, Usuario user2) {
        SwingUtilities.invokeLater(() -> {
            ChatUsuario chatUsuario = new ChatUsuario(user1, user2);
            chatUsuario.mainCall();
        });
    }
}

class ChatUsuario {
    private JFrame frame;
    private JTextField messageField;
    private JTextArea chatArea;
    private JButton sendButton;
    private JButton sendFileButton;
    private Socket socket;
    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;
    Usuario user1;
    Usuario user2;

    public ChatUsuario(Usuario user1, Usuario user2) {
        this.user1 = user1;
        this.user2 = user2;
    }

    private void initializeUI() {
        frame = new JFrame("Chat - " + user2.userName);
        frame.setBounds(100, 100, 400, 300);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout());

        chatArea = new JTextArea();
        chatArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(chatArea);
        frame.getContentPane().add(scrollPane, BorderLayout.CENTER);

        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new BorderLayout());

        messageField = new JTextField();
        inputPanel.add(messageField, BorderLayout.CENTER);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                closeConnection1();
            }
        });

        sendButton = new JButton("Enviar");
        sendButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                sendMessage();
            }
        });
        messageField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                sendMessage();
            }
        });
        inputPanel.add(sendButton, BorderLayout.EAST);

        sendFileButton = new JButton("Mandar Archivo");
        sendFileButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                sendFile();
            }
        });

        inputPanel.add(sendFileButton, BorderLayout.WEST);

        frame.getContentPane().add(inputPanel, BorderLayout.SOUTH);

        frame.setVisible(true);
    }

    private void initializeNetwork() {
        try {
            socket = new Socket(this.user2.ip, 5050);
    
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream = new ObjectInputStream(socket.getInputStream());
    
            Thread messageListener = new Thread(new Runnable() {
                public void run() {
                    while (true) {
                        try {
                            Object message = inputStream.readObject();
                            if (message instanceof String) {
                                handleStringMessage((String) message);
                            } else if (message instanceof FileTransfer) {
                                receiveFile((FileTransfer) message);
                            }
                        } catch (SocketException se) {
                            // Socket closed, no need to print the stack trace
                            break;
                        } catch (EOFException eofe) {
                            // End of file reached, no need to print the stack trace
                            break;
                        } catch (Exception e) {
                            e.printStackTrace();
                            break;  // Exit the loop when an exception occurs
                        }
                    }
                }
            });
            messageListener.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void closeConnection1() {
        try {
            // Send a special message to notify the server that the user chat is closing
            outputStream.writeObject("USER_CLOSING");
            outputStream.flush();
    
            // Interrupt the messageListener thread
            Thread.currentThread().interrupt();
    
            // Close the streams and socket immediately
            outputStream.close();
            inputStream.close();
            socket.close();
    
            // Exit the application
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleStringMessage(String message) {
        if (message.equals("SERVER_CLOSING")) {
            try{
                outputStream.writeObject("USER_CLOSING");
                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            closeConnection();
        } else {
            displayMessage(this.user2.userName + ": " + message);
        }
    }

    private void closeConnection() {
        try {
            // Close the streams
            outputStream.close();
            inputStream.close();
            
            // Check if the socket is still open before closing it
            if (!socket.isClosed()) {
                socket.close();
            }
    
            // Exit the application
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendMessage() {
        try {
            String message = messageField.getText();
            outputStream.writeObject(message);
            outputStream.flush();
            displayMessage(this.user1.userName + ": " + message);
            messageField.setText("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendFile() {
        JFileChooser fileChooser = new JFileChooser();
        int result = fileChooser.showOpenDialog(frame);

        if (result == JFileChooser.APPROVE_OPTION) {
            try {
                File selectedFile = fileChooser.getSelectedFile();
                byte[] fileData = new byte[(int) selectedFile.length()];
                FileInputStream fileInputStream = new FileInputStream(selectedFile);
                fileInputStream.read(fileData);

                FileTransfer fileTransfer = new FileTransfer(selectedFile.getName(), fileData);
                outputStream.writeObject(fileTransfer);
                outputStream.flush();

                displayMessage("Enviaste un archivo: " + selectedFile.getName());

                fileInputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void receiveFile(FileTransfer fileTransfer) {
        try {
            String fileName = fileTransfer.getFileName();
            byte[] fileData = fileTransfer.getFileData();
            FileDestination destination = new FileDestination();
            String path = destination.resPath();
            File receivedFile = new File(path + File.separator + "recibido_" + fileName);
            FileOutputStream fileOutputStream = new FileOutputStream(receivedFile);
            fileOutputStream.write(fileData);
            fileOutputStream.close();

            displayMessage("Archivo recibido: " + "recibido_" + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void displayMessage(String message) {
        chatArea.append(message + "\n");
    }

    public void mainCall() {
        initializeUI();
        initializeNetwork();
    }
}

class FileTransfer implements Serializable {
    private static final long serialVersionUID = 1L;
    private String fileName;
    private byte[] fileData;

    public FileTransfer(String fileName, byte[] fileData) {
        this.fileName = fileName;
        this.fileData = fileData;
    }

    public String getFileName() {
        return fileName;
    }

    public byte[] getFileData() {
        return fileData;
    }
}
