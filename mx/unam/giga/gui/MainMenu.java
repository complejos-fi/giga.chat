package mx.unam.giga.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.*;

public class MainMenu extends JFrame {
    private JTextField nameField;
    private JButton enterButton;

    public MainMenu(){ 
        setTitle("GIGA_CHAD");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 200);
        setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 1, 10, 10)); // Set grid layout with 3 rows, 1 column, and 10px spacing

        JLabel nameLabel = new JLabel("Ingresa tu nombre: ");
        nameField = new JTextField();
        enterButton = new JButton("Let's go!");

        enterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = nameField.getText();
                String ip = getIPAdress();
                Usuario user = new Usuario(name, ip);
                initSocialMenu socialMenu = new initSocialMenu(user);
            }
        });
        nameField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = nameField.getText();
                String ip = getIPAdress();
                Usuario user = new Usuario(name, ip);
                initSocialMenu socialMenu = new initSocialMenu(user);
            }
        });

        panel.add(nameLabel);
        panel.add(nameField);
        panel.add(enterButton);

        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10)); // Add padding to the panel

        add(panel);
        setVisible(true);
    }

    public String getIPAdress(){
        String ip = "";
        try {
            InetAddress address = InetAddress.getLocalHost();
            ip = address.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return ip;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainMenu();
            }
        });
    }
}
