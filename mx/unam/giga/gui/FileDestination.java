package mx.unam.giga.gui;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.concurrent.CountDownLatch;


public class FileDestination {
    public String resPath() {
        CountDownLatch latch = new CountDownLatch(1); // Crea un CountDownLatch con un contador inicial de 1
        String[] res = new String[1]; // Crea un arreglo de Strings para almacenar el resultado

        SwingUtilities.invokeLater(() -> { // Ejecuta el código en el hilo de eventos de Swing
            FileSender fileSender = new FileSender(); // Crea una instancia de FileSender
            fileSender.setVisible(true); // Hace visible el frame de FileSender

            fileSender.addSelectionListener(path -> { // Agrega un SelectionListener a FileSender
                res[0] = path; // Almacena la ruta seleccionada en el arreglo de resultados
                if (res[0] == null || res[0].isEmpty()) {
                    // Show an error message and prevent closing the window
                    fileSender.showError("Please select a valid directory path.");
                } else {
                    fileSender.dispose(); // Cierra el frame de FileSender
                    latch.countDown(); // Libera el CountDownLatch para desbloquear el hilo principal
                }
            });

            fileSender.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // Cierra la aplicación cuando se cierra el frame de FileSender
        });

        try {
            latch.await(); // Espera hasta que el CountDownLatch sea liberado
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (res[0] == null) {
            return ""; // Retorna una cadena vacía si no se seleccionó ningún archivo
        } else {
            return res[0]; // Retorna la ruta seleccionada
        }
    }
}

class FileSender extends JFrame {
    private File selectedDirectory; // Variable para almacenar el directorio seleccionado
    private String directoryPath;
    private SelectionListener selectionListener;

    public FileSender() {
        initComponents(); // Llama al método para crear el menú de archivos
    }

    public void addSelectionListener(SelectionListener listener) {
        this.selectionListener = listener;
    }

    private void initComponents() {
        setLayout(new GridLayout(2, 1)); // Establece el diseño del frame como una cuadrícula de 2x1

        JPanel directoryPanel = new JPanel(); // Crea un nuevo JPanel para contener los componentes relacionados con el directorio
        JLabel directoryLabel = new JLabel("Selecciona un directorio:"); // Crea un nuevo JLabel con el texto "Selecciona un directorio:"
        JTextField directoryField = new JTextField(20); // Crea un nuevo JTextField con un ancho de 20
        JButton directoryButton = new JButton("Examinar"); // Crea un nuevo JButton con el texto "Examinar"
        directoryButton.addActionListener(e -> { // Agrega un ActionListener al directoryButton
            JFileChooser fileChooser = new JFileChooser(); // Crea un nuevo JFileChooser
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); // Permite seleccionar solo directorios
            int result = fileChooser.showOpenDialog(this); // Muestra el diálogo del selector de archivos y obtén el resultado
            if (result == JFileChooser.APPROVE_OPTION) { // Si el usuario selecciona un directorio
                selectedDirectory = fileChooser.getSelectedFile(); // Obtiene el directorio seleccionado
                directoryField.setText(selectedDirectory.getAbsolutePath()); // Establece el texto de directoryField como la ruta absoluta del directorio seleccionado
            }
        });

        JButton selectButton = new JButton("Seleccionar"); // Crea un nuevo JButton con el texto "Seleccionar
        selectButton.addActionListener(new ActionListener() { // Agrega un ActionListener al selectButton
            @Override
            public void actionPerformed(ActionEvent e) {
                directoryPath = directoryField.getText(); // Obtiene la ruta del directorio seleccionado
                if (selectionListener != null) {
                    selectionListener.onSelection(directoryPath);
                }
            }
        });
        directoryField.setEditable(false); // Deshabilita la edición de directoryField
        directoryPanel.add(directoryLabel); // Agrega directoryLabel a directoryPanel
        directoryPanel.add(directoryField); // Agrega directoryField a directoryPanel
        directoryPanel.add(directoryButton); // Agrega directoryButton a directoryPanel
        directoryPanel.add(selectButton); // Agrega selectButton a directoryPanel

        add(directoryPanel); // Agrega directoryPanel al frame
        setVisible(true); // Hace visible el frame
        pack(); // Ajusta el tamaño del frame a su tamaño preferido

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // Cierra la aplicación cuando se cierra el frame de FileSender
    }

    public void showError(String message) {
        JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public interface SelectionListener {
        void onSelection(String path);
    }
}