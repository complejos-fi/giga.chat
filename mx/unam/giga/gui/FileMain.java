package mx.unam.giga.gui;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.concurrent.CountDownLatch;

public class FileMain {
    public String resPath() {
        CountDownLatch latch = new CountDownLatch(1); // Crea un CountDownLatch con un contador inicial de 1
        String[] res = new String[1]; // Crea un arreglo de Strings para almacenar el resultado

        SwingUtilities.invokeLater(() -> { // Ejecuta el código en el hilo de eventos de Swing
            FileSenderRenamed fileSender = new FileSenderRenamed(); // Crea una instancia de FileSender
            fileSender.setVisible(true); // Hace visible el frame de FileSender

            fileSender.addSelectionListener(path -> { // Agrega un SelectionListener a FileSender
                res[0] = path; // Almacena la ruta seleccionada en el arreglo de resultados
                fileSender.dispose(); // Cierra el frame de FileSender
                latch.countDown(); // Libera el CountDownLatch para desbloquear el hilo principal
            });

            fileSender.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // Cierra la aplicación cuando se cierra el frame de FileSender
        });

        try {
            latch.await(); // Espera hasta que el CountDownLatch sea liberado
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (res[0] == null) {
            return ""; // Retorna una cadena vacía si no se seleccionó ningún archivo
        } else {
            return res[0]; // Retorna la ruta seleccionada
        }
    }
}

class FileSenderRenamed extends JFrame {
    private File selectedFile; // Variable para almacenar el archivo seleccionado
    private String archivorespuesta;
    private SelectionListener selectionListener;

    public FileSenderRenamed() {
        initComponents(); // Llama al método para crear el menú de archivos
    }

    public void addSelectionListener(SelectionListener listener) {
        this.selectionListener = listener;
    }

    private void initComponents() {
        setLayout(new GridLayout(3, 1)); // Establece el diseño del frame como una cuadrícula de 3x1

        JPanel filePanel = new JPanel(); // Crea un nuevo JPanel para contener los componentes relacionados con el archivo
        JLabel fileLabel = new JLabel("Selecciona un archivo de texto:"); // Crea un nuevo JLabel con el texto "Selecciona un archivo de texto:"
        JTextField fileField = new JTextField(20); // Crea un nuevo JTextField con un ancho de 20
        JButton fileButton = new JButton("Examinar"); // Crea un nuevo JButton con el texto "Examinar"
        fileButton.addActionListener(e -> { // Agrega un ActionListener al fileButton
            JFileChooser fileChooser = new JFileChooser(); // Crea un nuevo JFileChooser
            int result = fileChooser.showOpenDialog(this); // Muestra el diálogo del selector de archivos y obtén el resultado
            if (result == JFileChooser.APPROVE_OPTION) { // Si el usuario selecciona un archivo
                selectedFile = fileChooser.getSelectedFile(); // Obtiene el archivo seleccionado
                fileField.setText(selectedFile.getAbsolutePath()); // Establece el texto de fileField como la ruta absoluta del archivo seleccionado
            }
        });

        JButton selectButton = new JButton("Seleccionar"); // Crea un nuevo JButton con el texto "Seleccionar
        selectButton.addActionListener(new ActionListener() { // Agrega un ActionListener al selectButton
            @Override
            public void actionPerformed(ActionEvent e) {
                archivorespuesta = filePath(); // Llama al método filePath
                if (selectionListener != null) {
                    selectionListener.onSelection(archivorespuesta);
                }
            }
        });

        filePanel.add(fileLabel); // Agrega fileLabel a filePanel
        filePanel.add(fileField); // Agrega fileField a filePanel
        filePanel.add(fileButton); // Agrega fileButton a filePanel
        filePanel.add(selectButton); // Agrega selectButton a filePanel

        add(filePanel); // Agrega filePanel al frame
        pack(); // Ajusta el tamaño del frame a su tamaño preferido
    }

    private String filePath() {
        if (selectedFile == null) {
            return ""; // Retorna una cadena vacía si no se seleccionó ningún archivo
        } else {
            String path = selectedFile.getAbsolutePath(); // Obtiene la ruta absoluta del archivo seleccionado
            return path;
        }
    }

    public interface SelectionListener {
        void onSelection(String path);
    }
}