package mx.unam.giga.gui;
import java.util.ArrayList;
import java.util.List;

public class Usuario {
    public String userName;
    public String ip;
    public final int port = 6600;
    List<Usuario> amigos;

    public Usuario(String userName, String ip) {
        this.userName = userName;
        this.ip = ip;
        this.amigos = new ArrayList<>();
    }

    public void Amigos(List<Usuario> amigos){
        this.amigos = amigos;
    }
}
