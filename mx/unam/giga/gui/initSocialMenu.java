package mx.unam.giga.gui;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import mx.unam.giga.chat.initChatServidor;
import mx.unam.giga.chat.initChatUsuario;

public class initSocialMenu {
    public initSocialMenu(Usuario user) {
        SwingUtilities.invokeLater(() -> {
            SocialMenu socialMenu = new SocialMenu(user);
            socialMenu.mainCall();
        });
    }
    
}

class SocialMenu extends JFrame {
    private LinkedList<Usuario> friendsList = new LinkedList<>();
    private List<String> friendsList1 = new ArrayList<>();
    private JList<String> friendsJList;
    Usuario user;

    public SocialMenu(Usuario user) {
        this.user = user;
    }

    public void initializeUI() {
        loadFriendsList();

        setTitle("Social Menu - " + user.userName);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());

        friendsJList = new JList<>(friendsList1.toArray(new String[0]));
        JButton addButton;
        JButton deleteButton;
        JButton chatButton;
        JScrollPane scrollPane = new JScrollPane(friendsJList);
        add(scrollPane, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new GridLayout(2, 1));

        chatButton = new JButton("Chat");
        chatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIndex = friendsJList.getSelectedIndex();
                if (selectedIndex != -1) {
                    Usuario friend = friendsList.get(selectedIndex);
                    
                    JFrame chatOptionsFrame = new JFrame("Opciones de chat");
                    chatOptionsFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    chatOptionsFrame.setLayout(new FlowLayout());

                    JButton hostButton = new JButton("Host");
                    hostButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            chatOptionsFrame.dispose();
                            initChatServidor chatServidor = new initChatServidor(user, friend.userName);
                        }
                    });
                    chatOptionsFrame.add(hostButton);

                    JButton joinButton = new JButton("Join");
                    joinButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            chatOptionsFrame.dispose();
                            initChatUsuario chatUsuario = new initChatUsuario(user, friend);
                        }
                    });
                    chatOptionsFrame.add(joinButton);
                    chatOptionsFrame.pack();
                    chatOptionsFrame.setLocationRelativeTo(null);
                    chatOptionsFrame.setVisible(true);
                }
            }
        });
        buttonPanel.add(chatButton);
        
        
        addButton = new JButton("Agregar amigo");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addFriend();
                refreshFriendsList();
            }
        });
        buttonPanel.add(addButton);
        add(buttonPanel, BorderLayout.SOUTH);
        

        deleteButton = new JButton("Borrar amigo");
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIndex = friendsJList.getSelectedIndex();
                if (selectedIndex != -1) {
                    refreshFriendsList();
                }
            }
        });
        add(deleteButton, BorderLayout.NORTH);
        pack();
        setLocationRelativeTo(null);
        setSize(300, 400);
        setVisible(true);
    }

    public void addFriend() {
        JFrame inputFrame = new JFrame("Agregar amigo");
        inputFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        inputFrame.setLayout(new BoxLayout(inputFrame.getContentPane(), BoxLayout.Y_AXIS));

        JTextField nameField = new JTextField();
        JTextField ipField = new JTextField();

        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String tmpName = nameField.getText();
                String tmpip = ipField.getText();
                if (tmpName != null && tmpip != null) {
                    Usuario toAdd = new Usuario(tmpName, tmpip);
                    friendsList.add(toAdd);
                    friendsList1.add(toAdd.userName);
                    saveFriendsList();
                    refreshFriendsList();
                    inputFrame.dispose();
                } else {
                    JOptionPane.showMessageDialog(inputFrame, "Porfavor ingrese un nombre e ip validos.");
                }
            }
        });

        inputFrame.add(new JLabel("Nombre: "));
        inputFrame.add(nameField);
        inputFrame.add(new JLabel("Dirección IP: "));
        inputFrame.add(ipField);
        inputFrame.add(okButton);
        inputFrame.pack();
        inputFrame.setSize(300, 150);
        inputFrame.setLocationRelativeTo(null);
        inputFrame.setVisible(true);
    }

    public int searchFriend(String friendName) {
        int index = -1;
        for (int i = 0; i < friendsList.size(); i++) {
            if (friendsList.get(i).userName.equals(friendName)) {
                index = i;
                break;
            }
        }
        return index;
    }

    private void loadFriendsList() {
        String filePath = "mx/unam/giga/Data/amigos.csv";
         try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                String[] userData = linea.split(",");
                Usuario tempUser = new Usuario(userData[0], userData[1]);
                friendsList.add(tempUser);
                friendsList1.add(tempUser.userName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void refreshFriendsList() {
        friendsJList.setListData(friendsList1.toArray(new String[0]));
        saveFriendsList();
    }

    private void saveFriendsList() {
        try (FileWriter writer = new FileWriter("mx/unam/giga/Data/amigos.csv")) {
            for (Usuario friend : friendsList) {
                writer.write(friend.userName + "," + friend.ip + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void mainCall() {
        initializeUI();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            Usuario user = new Usuario("John", "192.168.0.1");
            new SocialMenu(user);

        });
    }
}